import express from "express";
import bodyParser from "body-parser";
import * as mockedData from "../resources/MockedListData.json";

const app = express();
const port = 8080;

// parse application/x-www-form-urlencoded from body
app.use(bodyParser.json());

//parse application.json from body
app.use(bodyParser.urlencoded({ extended: false }));

app.get("/", (request, response) => {
	response.send("Hello world");
});

app.get("/greetings", (req, res) => {
	res.send(`Welcome John Doe!`);
});

app.get("/greetings/:name", (req, res) => {
	res.send(`Welcome ${req.params.name}!`);
});

app.get("/lists", (req, res) => {
	res.send(mockedData.lists);
});

app.get("/list/:id", (req, res) => {
	let idFromRequest = req.params.id;
	let requestList = mockedData.lists.find(el => el.id === idFromRequest);
	if (requestList) {
		res.send(requestList);
	} else {
		res.send("No list found.");
	}
});

app.get("/list/listElements/:category:id", (req, res) => {
	let firstId = req.params.category;
	let secondId = req.params.id;
	let result = {title:"", body:""};
	let requestList = mockedData.lists.find(el => el.id === firstId);
	let requestItem = requestList!.listElements.find(el => el.id === secondId);

	// // First method
	// result.title = requestItem.title;
	// result.body = requestItem.body;
	// //result.title = requestItem.map(function(title))
	// if (result) {
	// 	res.send(result);
	// } else {
	// 	res.send("No list found.");
	// }

	//Smartest method	
	
	if (requestItem.id) {
		delete requestItem.id;
		res.send(requestItem);
	} else {
		res.send("No list found.");
	}
});

//app.get("/list/:")

// 1 param "message" read from body
app.post("/toConsole", (req, res) => {
	console.log(req.body.message);
	if (req.body && req.body.message) {
		console.log(req.body.message);
		res.send("Message was posted to server.");
	} else {
		res.send("No message received. Nothing was posted on sever.");
	}
});

app.post("/addElement/:id", (req, res) =>  {
	let listId = req.params.id;
	console.log(listId);
	let targetList = mockedData.lists.find(el => el.id === listId);
	/*if(!req.body.message){
		res.send("Message invalid!");
		return;
	} */

	let input = req.body;
	let lists = mockedData.lists;
	//var inputText = JSON.parse(input);
	let list = lists.find(element => element.id === listId);
	list.listElements.push(input);
	var json = JSON.stringify(mockedData);
	var fs = require(`fs`);
	fs.writeFile(`C:/Users/FI073492/Desktop/Utils/course 18.07/resources/MockedListData.json`, json,
					function(err: any) {if(err){console.log(err)}});
	res.send("Object has been added.")
});

app.delete("/lists/listElement", (req, res) => {
	let listId = req.body.listId;
	let listElementId = req.body.listElementId;

	let list = mockedData.lists.find(list => list.id === listId);
	list.listElements = list.listElements.filter(listElement => listElement.id !== listElementId);
	
	res.send("List element deleted");
});

app.listen(port, () => {
	console.log(`Server started at http://localhost:${port}!`);
});
